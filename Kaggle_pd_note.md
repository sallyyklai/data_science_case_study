# Kaggle Pandas
* Initialisation: data frame or series
```python
dataFrameName = pd.DataFrame({header:[data]})
# optional:
# index: [list] of row-header, [0,1,...] by default
```
* Initialisation: from csv
```python
dataFrameName = pd.read_csv("file")
# optional:
# sep: ',' by default for csv
# skiprows: number of rows to skip from top
# na_values: list of null value to be identified as nan, eg [' ', 'na', 'no value']
# index_col: list of column number/or string of header to be identified as index if the csv originally has its own index, 0 usually
# use_cols: list of column number/or string of header to be imported
```
* Initialisation from sqlite
```python
import sqlite3
conn = sqlite3.connect("myDbFile.sqlite")
df = pd.read_sql_query("SELECT * FROM fires", conn)
```
* some basic properties of data frame
```python
df.shape #dimension
df.index # row header, usually [0,1,...]
df.columns # column headers
df.describe() # give mean, sd, count and quantiles for each
df.info() #data type info, memory info, count of row and columns and headers
```
* Selecting and indexing
```python
df[a:b] #row a to row b-1
df.header #or
df.['header'] #gives specific columns
df.head(n) #show top n rows, neglecting headers
df.iloc[[row],[column]] #row n column can only be integer, applicable no matter numeric or descriptive headers
df.loc[[index],[header]] # must match type of index and header
# in a nutshell: index and headers is mixture of numeric and descriptive then both works, if only descriptive then only use loc
```
* Conditional selecting
```python
df.loc[boolean condition] # gives rows that match condition, as first arg of loc takes rows
```
* Mapping
```python
mySeries.map(myFunction)
myDataFrame.apply(myFunction, axis='columns')
# myFunction should be function/lambda expression that take in one argument(single number for series, a row for data frame and return one value/row, in the case of axis is columns, otherwise reverse row/column)
```
* creating/dropping new columns
 - ```axis = 1```which is equivalent of ```axis = 'columns'```, and function should usually take a row as input and generate the new column by looking a the corresponding column field of a row, ie which is like generation new column through a function that take in some existing columns
  ```python
  df['new_column_name'] = df.apply(myFunction, axis=1)
  ```
 - or use
  ```python
  df = df.assign(new_column_name = some_series_or_array)
  ```
 - if ```inplace = False``` will return the column_to_drop, can include multiple columns in dropping list
 ```python
 df.drop(['column_to_drop'],inplace=True)
 ```
* Summary for categorical variables
```python
mySeries.unique() #array of unique values
mySeries.value_counts(sort=True) #Series with each category as index and count as data, order with descending count when sort=True
# make sure sort=False if binning with interval
```
* Grouping and sorting
```python
df.groupby('header') #or list of headers in arg gives slices of df to do all operation related to a data frame
df.groupby('header').header.agg([list of function]) #gives result of different functions with corresponding function column
df.sort_values(by='header') #ascending order true by default or by=[list of headers]
df.cut() #
df.qcut(df.column_to_be_cut,q,labels) #where q is number of result interval and labels are value corresponding to each interval, will get equal number of count in each interval given correct corresponding amount of data, while number of count in each interval in cut depends on how one define the inteval boundary
```
* Missing values
```python
df.isnull() #return boolean in all spaces
df[df.header.isnull()] #return rows that contain null
df.isnull().any() #return headers as index and boolean for whether that columns contain null
df.columns[df.isnull().any()].tolist() #gives list of headers with null
```
* Renaming and combining
```python
df.rename(index={origin: to-be-replaced}) #index could also be columns
pd.concat([df1,df2]) #union two df with same columns, vertically
leftDf = left.set_index([common headers])
leftDf = right.set_index([common headers])
leftDf.join(rightDf,lsuffix = 'string append to headers of leftdf',rsuffix = 'string append to headers of rightdf') #join two df with same index horizontally
```
