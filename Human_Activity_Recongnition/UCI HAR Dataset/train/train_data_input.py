import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

def data_import():
    headers = pd.read_csv("../features.txt",delim_whitespace=True,usecols=[1],squeeze=True,header=None)
    label = pd.read_csv("y_train.txt",delim_whitespace=True,squeeze=True,header=None)
    df = pd.read_csv("X_train.txt",delim_whitespace=True,names=headers,header=None)
    # mangle_dupe_cols = True by default, so duplicates in headers are named X, X.1, X.2 ... etc
    df["Label"] = label
    return df

def data_brief(df):
    print("Columns with null: ", df.columns[df.isnull().any()].tolist())

def plot_corr(df):
    f, ax = plt.subplots(figsize=(10, 6))
    corr = df.corr().iloc[:50,:50]
    hm = sns.heatmap(round(corr,2), annot=True, ax=ax, cmap="coolwarm",fmt='.2f',linewidths=.05,annot_kws={"size": 5})
    f.subplots_adjust(top=0.93)
    t= f.suptitle('Correlation Heatmap', fontsize=14)
    plt.show()

# if __name__ == '__main__':
    # df = data_import()
    # # data_brief(df)
    # plot_corr(df)
