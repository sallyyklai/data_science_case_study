from sklearn import datasets
from sklearn.decomposition import PCA
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
iris = datasets.load_iris()
# df = pd.DataFrame(iris.data,columns = iris.feature_names)
# df["target"] = pd.Series(iris.target)
# df["type"] = pd.cut(df["target"],[-0.1,0.9,1.9,2.9],labels=["setosa","versicolor","virginica"])

# fig = plt.figure(1, figsize=(8, 6))
# ax = Axes3D(fig, elev=-150, azim=110)
X_reduced = PCA(n_components=2).fit_transform(iris.data)
plt.scatter(X_reduced[:, 0], X_reduced[:, 1], c=iris.target,
           cmap=plt.cm.Set1, edgecolor='k', s=40)
plt.title("First three PCA directions")
# ax.set_xlabel("1st eigenvector")
# ax.w_xaxis.set_ticklabels([])
# ax.set_ylabel("2nd eigenvector")
# ax.w_yaxis.set_ticklabels([])
# ax.set_zlabel("3rd eigenvector")
# ax.w_zaxis.set_ticklabels([])

plt.show()
