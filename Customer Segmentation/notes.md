# Customer segmentation
* Chapter 1: cohorts analysis
 - cohorts can be separated by: time, behaviour, size, this chapter mainly gives example on time cohort
 - a pivot table will have cohort in rows and cohort index in columns, cohort index ONE inner properties the cohort row grouping, metrics in the table
 - note the cohort(rows) and cohort index exist as a column in the origin data set
 - pivot table is obtain by ```df.groupby(['cohort row','cohort index'])```
 - example here: row: initial acquire month; column: month since acquisition; metric is number of customer belong to that grouping
 - at time analysis: one can acquire retention table
 - after grouping, one can look at some other properties among groups, eg: average buying quantity in that group
* Chapter 2: intro to RFM (Recency, frequency, monetary) segmentation
 1. get RFM for each customer by aggregating their total purchase time n value
 2. bin each customer's RFM (give them a band for each RFM)
    - for each recency, frequency and monetary value, customer can be grouped by percentiles, Pareto 80/20 cut or customise criteria, this chapter will focus on percentiles
    - ```df.qcut(df.column_to_be_cut,q,labels)``` where q is number of interval, which will be 4 for quantiles and labels are value correspond for each interval
 3. get each customer's RFM score by summing each of their RFM band value
 4. cut segment by score
 5. For each segment, get summary stats by: aggregate on mean of RGM as well as count of monetary value (which number of customer with that score)
    ```python
    df.groupby('RFM_Score').agg({'Recency': 'mean','Frequency': 'mean','MonetaryValue': ['mean', 'count']}).round(1)
    ```
