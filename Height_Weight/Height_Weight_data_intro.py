import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import pearsonr

def data_import():
    height_weight, = pd.read_html("http://socr.ucla.edu/docs/resources/SOCR_Data/SOCR_Data_Dinov_020108_HeightsWeights.html",index_col=0,header=0)
    return height_weight

def data_brief(df):
    df.info()
    print("-"*50)
    print(df.describe())
    print("-"*50)
    print("Columns with null: ", df.columns[df.isnull().any()].tolist())

def boxplot(df):
    df.boxplot()
    plt.show()

def binningHistogram(df,header):
    bins = np.linspace(df[header].min(),df[header].max(),8)
    bin_width = bins[1] - bins[0]
    binned = pd.cut(df[header],bins).value_counts(sort=False)
    plt.bar(bins[1:] - (bin_width/2),binned,bin_width)
    plt.xticks(bins)
    plt.xlim(bins[0],bins[-1])
    plt.xlabel(header)
    plt.ylabel("Count")
    plt.title("Histogram of " + header)
    plt.show()
    # df.hist(bins=8,grid=False) #would do the work but as well

def scatterCorr(df):
    plt.scatter(df["Height(Inches)"],df["Weight(Pounds)"],marker="x",s=10)
    plt.xlabel("Height(Inches)")
    plt.ylabel("Weight(Pounds)")
    plt.show()
    print("Correlation = {0}".format(round(pearsonr(df["Height(Inches)"],df["Weight(Pounds)"])[0],3)))

if __name__ == '__main__':
    df = data_import()
    # data_brief(df)
    # boxplot(df)
    # binningHistogram(df,"Height(Inches)")
    # binningHistogram(df,"Weight(Pounds)")
    # scatterCorr(df)
